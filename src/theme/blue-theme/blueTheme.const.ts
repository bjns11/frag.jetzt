export const blue = {
  '--primary' : '#509556',
  '--primary-variant': '#3e5a7b',

  '--secondary': '#7fc682',
  '--secondary-variant': '#4d2917',

  '--background': '#4f342b',
  '--surface': '#260f01',
  '--dialog': '#260f01',
  '--cancel': '#50a554',
  '--alt-surface': '#3d2617',
  '--alt-dialog': '#3d2617',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#ffffff',
  '--on-surface': '#ffffff',
  '--on-cancel': '#000000',

  '--green': '#3cc400',
  '--red': 'red',
  '--yellow': 'gold',
  '--blue': '#79a8e2',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': '#aaaaaa',
  '--grey-light': '#aaaaaa',
  '--black': 'black',
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Nature Mode',
      'de': 'Nature Mode'
    },
    'description': {
      'en': 'Forest colors for a natural look',
      'de': 'Angenehme Waldfarben'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
